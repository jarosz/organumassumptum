#include "Common.h"

long ORGAN_STATE = 0;

using namespace std;

void zeropad(unsigned long value, unsigned long position, unsigned long base)
{
  for (unsigned long i = 1, comparison = base; i < position; ++i, comparison *= base) {
    if (value < comparison) {
      DEBUG_PRINT('0');
    }
  }
}