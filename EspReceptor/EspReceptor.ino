#include <ESP8266WiFi.h>
#include <MIDI.h>
#include <elapsedMillis.h>

#include "Common.h"
#include "Credentials.h"

WiFiUDP Udp;

MidiInterface StandardMidi(Serial1);
UdpMidiInterface UdpMidi(Udp);

void all_notes_off_channel(midi::Channel channel)
{
  const int note_min = 36;
  const int note_max = (channel == 4) ? 62 : 89;
  for (int i = note_min; i <= note_max; ++i) {
    StandardMidi.sendNoteOff(i, 64, channel);
  }
}

void all_notes_off()
{
  all_notes_off_channel(2);
  all_notes_off_channel(1);
  all_notes_off_channel(4);
}

void all_switches_off()
{
  const int switches_count = sizeof(ORGAN_SWITCHES);
  for (int i = 0; i < switches_count; ++i) {
    StandardMidi.sendControlChange(ORGAN_SWITCHES[i], 0, 1);
  }
}

void midi_panic()
{
  all_switches_off();
  all_notes_off();
}

void handle_note_on(midi::Channel channel, midi::DataByte note, midi::DataByte velocity)
{
  const int note_min = 36;
  const int note_max = (channel == 4) ? 62 : 89;
  if (note >= note_min && note <= note_max) {
    StandardMidi.sendNoteOn(note, 64, channel);
  }
  Serial.printf("NoteOn[%02d]  %3d %3d\n", channel, note, velocity);
}

void handle_note_off(midi::Channel channel, midi::DataByte note, midi::DataByte velocity)
{
  const int note_min = 36;
  const int note_max = (channel == 4) ? 62 : 89;
  if (note >= note_min && note <= note_max) {
    StandardMidi.sendNoteOn(note, 0, channel);
    // StandardMidi.sendNoteOn(note, 0, channel);
  }
  Serial.printf("NoteOff[%02d] %3d %3d\n", channel, note, velocity);
}

void handle_control_change(midi::Channel channel, midi::DataByte number, midi::DataByte value)
{
  StandardMidi.sendControlChange(number, value, channel);
  Serial.printf("CC%-3d[%02d]     %3d\n", number, channel, value);
}

void handle_active_sensing()
{
  Serial.println("ActiveSensing");
}

void set_midi_handlers()
{
  UdpMidi.setHandleNoteOn(handle_note_on);
  UdpMidi.setHandleNoteOff(handle_note_off);
  UdpMidi.setHandleControlChange(handle_control_change);
  UdpMidi.setHandleActiveSensing(handle_active_sensing);
}

void setup()
{
  Serial.begin(74880);

  StandardMidi.begin(MIDI_CHANNEL_OMNI);
  StandardMidi.turnThruOff();

  delay(10);
  Serial.println();
  Serial.printf("EspReceptor, build %s %s\n", __DATE__, __TIME__);


  WiFi.mode(WIFI_STA);
  WiFi.disconnect(); // For sure

  WiFi.mode(WIFI_STA);
  WiFi.config(RECEPTOR_IP, GATEWAY_IP, SUBNET_MASK);

  Serial.print("Connecting to ");
  Serial.println(WIFI_SSID);

  WiFi.begin(WIFI_SSID, WIFI_PASS);

  int i = 0;
  do {
    Serial.print('.');
    delay(500);    
  }
  while (WiFi.status() != WL_CONNECTED && i++ < 20);
  if (WiFi.status() != WL_CONNECTED) {
    WiFi.disconnect(); // For sure
    WiFi.mode(WIFI_AP);
    Serial.println("Setting soft-AP");
    Serial.print("SSID:");
    Serial.println(WIFI_SSID_FALLBACK); // const char* const WIFI_SSID = "********";
    Serial.print("pass:");
    Serial.println(WIFI_PASS_FALLBACK); // const char* const WIFI_PASS = "********";

    Serial.println(WiFi.softAPConfig(RECEPTOR_IP, GATEWAY_IP, SUBNET_MASK) ? "Ready" : "Failed!");

    Serial.println(WiFi.softAP(WIFI_SSID_FALLBACK, WIFI_PASS_FALLBACK, 8, true) ? "Ready" : "Failed!");
  }

  Serial.printf("Listening at %s:%d\n", RECEPTOR_IP.toString().c_str(), UDP_PORT);


  // Serial.print("Connecting to ");
  // Serial.println(wifiSsid);
  
  // WiFi.config(RECEPTOR_IP, GATEWAY_IP, SUBNET_MASK);
  // WiFi.mode(WIFI_STA);
  // WiFi.begin(wifiSsid, wifiPass);

  // while (WiFi.status() != WL_CONNECTED) {
  //   delay(500);
  //   Serial.print(".");
  // }

  // Serial.println("");
  // Serial.println("WiFi connected");

  // Serial.printf("Listening at %s:%d\n", WiFi.localIP().toString().c_str(), UDP_PORT);


  UdpMidi.begin(MIDI_CHANNEL_OMNI);
  UdpMidi.turnThruOff();
  // Udp.begin(UDP_PORT);

  set_midi_handlers();

  midi_panic();
}

void loop()
{
  static elapsedMillis since_last_message;
  const unsigned long last_message_timeout = 1000;
  static bool timed_out = true;

  if (!timed_out && since_last_message >= last_message_timeout) {
    midi_panic();
    Serial.println("Timeout");
    timed_out = true;
  }

  if (Udp.parsePacket()) { // Try 1 byte parsing and Loop while !read? / change behaviour depending on 1ByteParsing
    while(Udp.available()) {
      UdpMidi.read();
    }
    // Serial.printf("Received MIDI data from %s, port %d\n", Udp.remoteIP().toString().c_str(), Udp.remotePort());
    since_last_message = 0;
    timed_out = false;      
  }
}