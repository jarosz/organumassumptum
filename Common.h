#pragma once

#include <MIDI.h>
#include <WiFiUdp.h>

extern WiFiUDP Udp;

extern Stream &DEBUG_STREAM;

#define DEBUG

#ifdef DEBUG

#define DEBUG_PRINT(s) DEBUG_STREAM.print(s)
#define DEBUG_PRINTLN(str) DEBUG_STREAM.println(str)
#define DEBUG_PRINTBIN(s) DEBUG_STREAM.print(s, BIN)
#define DEBUG_PRINTDEC(s) DEBUG_STREAM.print(s, DEC)
#define DEBUG_PRINTHEX(s) DEBUG_STREAM.print(s, HEX)
#define DEBUG_ZEROPAD(n, l, p) zeropad(n, l, p)
#define DEBUG_PRINT_HEADER() \
  DEBUG_STREAM.print(millis()); \
  DEBUG_STREAM.print(F(": ")); \
  DEBUG_STREAM.print(__PRETTY_FUNCTION__); \
  DEBUG_STREAM.print(' '); \
  DEBUG_STREAM.print(__FILE__); \
  DEBUG_STREAM.print(':'); \
  DEBUG_STREAM.print(__LINE__); \
  DEBUG_STREAM.print(' ')

#define DEBUG_PRINTH(str) \
  DEBUG_PRINT_HEADER(); \
  DEBUG_PRINTLN(str)

#else

#define DEBUG_PRINT(str)

#endif

const IPAddress RECEPTOR_IP(192, 168, 1, 197); // set gateway to match your network
const IPAddress DATOR_IP(192, 168, 1, 198); // where xx is the desired IP Address
const IPAddress GATEWAY_IP(192, 168, 1, 1); // set gateway to match your network
const IPAddress SUBNET_MASK(255, 255, 255, 0); // set subnet mask to match your

// const char* const UDP_ADDRESS = "192.168.8.150";
const int UDP_PORT = 3210;

struct MidiSettings
{
  static const bool UseRunningStatus = true;
  static const bool HandleNullVelocityNoteOnAsNoteOff = true;
  static const bool Use1ByteParsing = true;
  static const long BaudRate = 31250;
  static const unsigned SysExMaxSize = 0;
};

struct SerialMidiSettings
: public MidiSettings
{
  static const bool UseRunningStatus = true;
  static const bool HandleNullVelocityNoteOnAsNoteOff = true;
  static const long BaudRate = 115200;
};

struct UdpMidiSettings
: public MidiSettings
{
  static const bool UseRunningStatus = true;
  static const bool HandleNullVelocityNoteOnAsNoteOff = true;
  static const long BaudRate = UDP_PORT; // Changed for port - 
};

struct SYSEX_START
{
  static const byte start = 0xF0;
  static const byte manufacturer = 0x2B;
  static const byte device_id = 0x02;
  static const byte model_id = 0x01;
  static const byte sending = 0x12;
  static const byte address1 = 0;
  static const byte address2 = 0;
  static const byte address3 = 0;
  // 000000 means JSON
};

struct SYSEX_STOP
{
  static const byte checksum = 0; // not implemented yet;
  static const byte stop = 0xF7;
};

typedef midi::MidiInterface<HardwareSerial, MidiSettings> MidiInterface;
typedef midi::MidiInterface<HardwareSerial, SerialMidiSettings> SerialMidiInterface;
typedef midi::MidiInterface<WiFiUDP, UdpMidiSettings> UdpMidiInterface;

extern MidiInterface StandardMidi;
extern SerialMidiInterface SerialMidi;
extern UdpMidiInterface UdpMidi;

const char ORGAN_SWITCHES[32] = {
// const std::array<char, 32> ORGAN_SWITCHES = {
      // Pedal (channel 4)
  72, // 1. Violon 16'
  73, // 2. Pryncypał 16'
  74, // 3. Subbass 16'
  75, // 4. Quinte 10 2/3'
  76, // 5. Oktawbas 8'
  77, // 6. Violon 8'
  78, // 7. Puzon 16'
  65, // 8. Koppel I-PED
  66, // 9. Koppel II-PED
      // Manual 1 (channel 2)
  79, // 10. Burdon 10'
  80, // 11. Pryncypał  8'
  81, // 12. Gamba 8'
  82, // 13. Flet 8'
  83, // 14. Quinta 2 2/3'
  84, // 15. Oktawa 4'
  85, // 16. Szpicflet 4'
  86, // 17. Flet leśny 2'
  87, // 18. Mixtura 3-ch'
  88, // 19. Kornet 2-ch'
  89, // 20. Trąbka 8'
  69, // 21. Koppel II-I
      // Manual 2 (channel 1)
  90, // 22. Gamba 8'
  91, // 23. Salicjonał 8'
  92, // 24. Gemshorn 8'
  93, // 25. Flet kryty 8'
  94, // 26. Pryncypał skrzypcowy 4'
  95, // 27. Flet jakiś 4'
  96, // 28. Oktawa 2'
  97, // 29. Progresja 2-ch
  98, // 30. Klarnet 8'
  70, // 31. Subkoppel II
  67, // 32. Tremolo
};

void set_midi_handlers();
void set_organ_switch(int number, bool state);
void set_organ_switches(unsigned long state);

void handle_note_on(midi::Channel channel, midi::DataByte note, midi::DataByte velocity);
void send_note_on(midi::Channel channel, midi::DataByte note, midi::DataByte velocity);
void handle_note_off(midi::Channel channel, midi::DataByte note, midi::DataByte velocity);
void send_note_off(midi::Channel channel, midi::DataByte note, midi::DataByte velocity);
void handle_control_change(midi::Channel channel, midi::DataByte note, midi::DataByte velocity);
void send_control_change(midi::Channel channel, midi::DataByte note, midi::DataByte velocity);
void handle_active_sensing();
void send_active_sensing();

void zeropad(unsigned long value, unsigned long position, unsigned long base = 10);
