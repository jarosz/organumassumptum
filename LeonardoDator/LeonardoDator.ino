#include <MIDI.h>
// #include <AltSoftSerial.h>
// #include <usbhub.h>
// #include <usbh_midi.h>
#include <U8g2lib.h>

class DummyMidiClass {
public:
  void sendControlChange(byte inControlNumber, byte inControlValue, byte inChannel);
};

void DummyMidiClass::sendControlChange(byte inControlNumber, byte inControlValue, byte inChannel) {

}

DummyMidiClass DummyMidi;

U8G2_SH1106_128X64_NONAME_F_4W_HW_SPI u8g2(U8G2_R0, 7, 6, 5);

#include "Common.h"
#include "Common.hpp"

#include "Music.h"
#include "Control.h"

Stream &DEBUG_STREAM = Serial;

// AltSoftSerial altSerial;
// typedef midi::MidiInterface<AltSoftSerial, MidiSettings> AltMidiInterface;

// AltMidiInterface AltMidi(altSerial);
// SerialMidiInterface Serial1Midi(Serial1);



void handle_note_on(midi::Channel channel, midi::DataByte note, midi::DataByte velocity)
{
  switch (note) {
    case 102:
      set_organ_switches(0b00001111111100011011110110010111, DummyMidi);
      break;
    case 103:
      set_organ_switch(20, !bitRead(ORGAN_STATE, 20), DummyMidi);
      break;
    case 104:
      set_organ_switches(0b00001111111100001011110110010111, DummyMidi);
      break;
    case 105:
      set_organ_switch(31, !bitRead(ORGAN_STATE, 31), DummyMidi);
      break;
    case 106:
      set_organ_switches(0b00111111111101011011110110110111, DummyMidi);
      break;
    case 107:
      set_organ_switches(0, DummyMidi);
      break;
    case 108:
      set_organ_switches(0b01011111111101111111110111111111, DummyMidi);
      break;
    default:
      // DummyMidi.sendNoteOn(note, velocity, map_channel(channel));
      break;
  }
  // DEBUG_PRINT(F("NoteOn["));
  // DEBUG_PRINTDEC(channel);
  // DEBUG_PRINT(F("]:"));
  // DEBUG_PRINTDEC(note);
  // DEBUG_PRINT(F(" "));
  // DEBUG_PRINTDEC(velocity);
  // DEBUG_PRINTLN();
}

void handle_note_off(midi::Channel channel, midi::DataByte note, midi::DataByte velocity)
{
  // Serial1Midi.sendNoteOff(note, velocity, map_channel(channel));

  // DEBUG_PRINT(F("NoteOff["));
  // DEBUG_PRINTDEC(channel);
  // DEBUG_PRINT(F("]:"));
  // DEBUG_PRINTDEC(note);
  // DEBUG_PRINT(F(" "));
  // DEBUG_PRINTDEC(velocity);
  // DEBUG_PRINTLN();
}

void handle_system_exclusive(byte *array, byte size)
{
  unsigned long address = 0;
  address |= array[5];
  address <<= 8;
  address |= array[6];
  address <<= 8;
  address |= array[7];

  // interpret as JSON
  if (address == 0) {
    // DEBUG_PRINTLN(reinterpret_cast<const char*>(array + 8));
  }
  // DatorInfo &di = *reinterpret_cast<DatorInfo*>(array);
  // DatorInfo *di = reinterpret_cast<DatorInfo*>(array);

  // Serial.print("[SysEx] ");
  // Serial.print("IP: ");
  // Serial.print(di->localIP);
  // Serial.print(" Status: ");
  // Serial.print(di->status);
  // Serial.print(" RSSI: ");
  // Serial.print(di->rssi);
  // Serial.println();
  //
  // Serial.println("SysEx:");
  // for (byte i = 0; i < size; ++i) {
  //   Serial.print(array[i], HEX);
  //   Serial.print(" ");
  // }
  // Serial.println();
}

void set_midi_handlers()
{
  // AltMidi.setHandleNoteOn(handle_note_on);
  // AltMidi.setHandleNoteOff(handle_note_off);

  // Serial1Midi.setHandleSystemExclusive(handle_system_exclusive);
}

// byte hex_ascii_to_nibble(byte ascii)
// {
//   byte nibble = 0;
//   if (ascii >= 48 && ascii <= 57) nibble = ascii - 48;
//   else if (ascii >=65 && ascii <= 70) nibble = ascii - 65 + 0xa;
//   else if (ascii >= 97&& ascii <= 102) nibble = ascii - 97 + 0xa;
//   else {}//error
//   return nibble;
// }

// char hex_ascii_to_databyte(byte ascii_upper, byte ascii_lower){
//   byte upper = ((hex_ascii_to_nibble(ascii_upper) << 4) & 0xFF);
//   byte lower = hex_ascii_to_nibble(ascii_lower);
//   return upper | lower;
// }

void setup()
{
  Serial.begin(74880);

  randomSeed(analogRead(0));

  while(!Serial);

  Serial.println(F("ASDF"));

  delay( 200 );

  // HidComposite.SetReportParser(0, &KbdPrs);
  // HidKeyboard.SetReportParser(0, &KbdPrs);
  // HidKeyboard2.SetReportParser(0, &KbdPrs);

  // Serial1Midi.begin(MIDI_CHANNEL_OMNI);
  // Serial1Midi.turnThruOff();
  
  // AltMidi.begin(MIDI_CHANNEL_OMNI);
  // AltMidi.turnThruOff();

  delay(10);

  set_midi_handlers();

  u8g2.begin();
}

void loop()
{
  static bool even = true;
  static byte ascii_upper = 0;

  // Handle MIDI input
  // if (AltMidi.read()) {
  //   last_message_timestamp = current_timestamp;
  // }

  // Handle Serial input
  // if (Serial1Midi.read()) {
  // }

  while (Serial.available()) {
    int user_input = Serial.read();
    Serial.print(user_input, HEX);
    if (!isWhitespace(user_input) && !isControl(user_input)) {
      switch (user_input) {
        case '0': OrganControl::set_switches(0); break;
        case '1': OrganControl::load_switches(0); break;
        case '!': OrganControl::save_switches(0); break;
        case '2': OrganControl::load_switches(1); break;
        case '@': OrganControl::save_switches(1); break;
        case '-': OrganControl::transpose_down(); break;
        case '+': OrganControl::transpose_up(); break;
        case '=': OrganControl::transpose_reset(); break;
        default: OrganControl::set_switch(random(32), random(2)); break;
      }
    }
  }
  

  // Handle terminal input
  // if (Serial.available()) {
  //   byte user_input = Serial.read();
  //   if (isHexadecimalDigit(user_input)) {
  //     if (even) {
  //       ascii_upper = user_input;
  //     }
  //     else {
  //       byte ascii_lower = user_input;
  //       byte midi_byte = hex_ascii_to_databyte(ascii_upper, ascii_lower);
  //       Serial1.write(midi_byte);
  //       last_message_timestamp = current_timestamp;
  //     }
  //     even = !even;      
  //   }
  // }


}