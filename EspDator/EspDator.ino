#include <elapsedMillis.h>
#include <MIDI.h>
#include <ESP8266WiFi.h>
#include <usbhub.h>
// #include <AltSoftSerial.h>

#include "Common.h"
#include "Common.hpp"

#include "Music.h"
#include "Control.h"
#include "Display.h"

#include "Credentials.h"

Stream &DEBUG_STREAM = Serial;

static elapsedMillis ms_since_active_sensing;
static elapsedMillis ms_since_ping;

static elapsedMicros us_since_packet;


typedef midi::MidiInterface<MidiUsb, MidiSettings> UsbMidiInterface;

WiFiUDP Udp;

typedef std::array<byte, 4> MidiMessage;
typedef std::vector<MidiMessage> MidiMessageVector;

static MidiMessageVector message_buffer;

// AltSoftSerial altPSerial;

// SerialMidiInterface SerialMidi(Serial);
UdpMidiInterface UdpMidi(Udp);

USB     Usb;
USBHub     Hub(&Usb);

HIDBoot<USB_HID_PROTOCOL_KEYBOARD>    HidKeyboard(&Usb);

MidiUsb UsbMidiWrapper(&Usb);

UsbMidiInterface UsbMidi(UsbMidiWrapper);

KbdRptParser KbdPrs;

midi::Channel map_channel(midi::Channel channel)
{
  switch (channel) {
    case 1: return 2;
    case 2: return 1;
  }
  return channel;
}

void handle_note_on(midi::Channel channel, midi::DataByte note, midi::DataByte velocity)
{
  send_note_on(channel, note, velocity);
  // Serial.printf("NoteOn[%02d]  %3d %3d\n", channel, note, velocity);
}

void send_note_on(midi::Channel channel, midi::DataByte note, midi::DataByte velocity)
{
  int pedal_value = OrganControl::get_pedal_value();
  if (pedal_value >= 0) {
    if (note <= 36 + pedal_value) {
      channel = 4;
    }
  }
  message_buffer.push_back({midi::NoteOn, note + OrganControl::get_transposition(), velocity, map_channel(channel)});
  // UdpMidi.sendNoteOn(note + OrganControl::get_transposition(), velocity, map_channel(channel));
  // ms_since_active_sensing = 0;  
}

void handle_note_off(midi::Channel channel, midi::DataByte note, midi::DataByte velocity)
{
  // Udp.beginPacket(RECEPTOR_IP, UDP_PORT);
  send_note_off(channel, note, velocity);
  // Udp.endPacket();
  // Serial.printf("NoteOff[%02d] %3d %3d\n", channel, note, velocity);
}

void send_note_off(midi::Channel channel, midi::DataByte note, midi::DataByte velocity)
{
  int pedal_value = OrganControl::get_pedal_value();
  if (pedal_value >= 0) {
    if (note <= 36 + pedal_value) {
      channel = 4;
    }
  }

  // Serial.printf("NoteOff[%02d] %3d %3d\n", channel, note, velocity);

  // UdpMidi.sendNoteOff(note + OrganControl::get_transposition(), velocity, map_channel(channel));
  // + Use running status and 0 velocity note on as note off
  message_buffer.push_back({midi::NoteOn, note + OrganControl::get_transposition(), 0, map_channel(channel)});
  // UdpMidi.sendNoteOn(note + OrganControl::get_transposition(), 0, map_channel(channel));
  // ms_since_active_sensing = 0;
}

void handle_control_change(midi::Channel channel, midi::DataByte number, midi::DataByte value)
{
  // Udp.beginPacket(RECEPTOR_IP, UDP_PORT);
  send_control_change(channel, number, value);
  // Udp.endPacket();
  // Serial.printf("CC%-3d[%02d]     %3d\n", number, channel, value);
}

void send_control_change(midi::Channel channel, midi::DataByte number, midi::DataByte value)
{
  message_buffer.push_back({midi::ControlChange, number, value, channel});
  // UdpMidi.sendControlChange(number, value, channel);
  // ms_since_active_sensing = 0;
}

void handle_active_sensing()
{
  // Udp.beginPacket(RECEPTOR_IP, UDP_PORT);
  send_active_sensing();
  // Udp.endPacket();
  // Serial.println("ActiveSensing");
}

void send_active_sensing()
{
  message_buffer.push_back({midi::ActiveSensing, 0, 0, 1});
  // UdpMidi.sendRealTime(midi::ActiveSensing);
  ms_since_active_sensing = 0;
}

void set_midi_handlers()
{
  // SerialMidi.setHandleNoteOn(handle_note_on);
  // SerialMidi.setHandleNoteOff(handle_note_off);
  // SerialMidi.setHandleControlChange(handle_control_change);
  // SerialMidi.setHandleActiveSensing(handle_active_sensing);

  UsbMidi.setHandleNoteOn(handle_note_on);
  UsbMidi.setHandleNoteOff(handle_note_off);
}

void send_packet()
{
  Udp.beginPacket(RECEPTOR_IP, UDP_PORT);
  for (auto message : message_buffer) {
    UdpMidi.send(
      static_cast<midi::MidiType>(message[0]),
      static_cast<midi::DataByte>(message[1]),
      static_cast<midi::DataByte>(message[2]),
      static_cast<midi::Channel>(message[3])
    );
    // Serial.print(message[0]);
    // Serial.print(" ");
    // Serial.print(message[1]);
    // Serial.print(" ");
    // Serial.print(message[2]);
    // Serial.print(" ");
    // Serial.println(message[3]);
  }
  Udp.endPacket();
  message_buffer.clear();
  us_since_packet = 0;
}

static Display display;

int connect_wifi(const char* ssid, const char* pass)
{
  WiFi.begin(ssid, pass);
  String wifi_message = String("Connecting WiFi: ") + ssid;

  display.info(wifi_message);

  int i = 0;
  do {
    wifi_message += '.';
    Usb.Task();
    display.info(wifi_message);
    delay(500);    
  }
  while (WiFi.status() != WL_CONNECTED && i++ < 20);
  return WiFi.status();
}

void setup()
{
  // SerialMidi.begin(MIDI_CHANNEL_OMNI);
  // pinMode(3, FUNCTION_3);
  // pinMode(1, FUNCTION_3);
  // pinMode(1, FUNCTION_3);

  // SerialMidi.turnThruOff();

  Serial.begin(74880);
  // Serial.begin(74880, SERIAL_8N1, SERIAL_TX_ONLY);

  // pinMode(3, FUNCTION_3);


  OrganControl::set_switches(0);

  display.setup();
  display.draw();

  // delay(1000);

  // Serial.println();
  // Serial.printf("EspDator, build %s %s\n", __DATE__, __TIME__);
  // Serial.print("Connecting to ");
  // Serial.println(WIFI_SSID);

  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  WiFi.mode(WIFI_STA);
  WiFi.config(DATOR_IP, GATEWAY_IP, SUBNET_MASK);
  
  display.draw();

  int res = Usb.Init();
  // Serial.println(res);
  if (res == -1) {
    display.error(String(F("OSC did not start. USB not working.")));
    delay(5000);
    display.clear_message();
  }
  else {
    delay(200);

    HidKeyboard.SetReportParser(0, &KbdPrs);

    UsbMidi.begin(MIDI_CHANNEL_OMNI);
    UsbMidi.turnThruOff();
    Usb.Task();
  }

  if (connect_wifi(WIFI_SSID, WIFI_PASS) != WL_CONNECTED) {
    WiFi.disconnect();
    display.error(String("Can't connect to WiFi ") + WIFI_SSID + ". Trying fallback network in 5 seconds.");
    delay(5000);
    if (connect_wifi(WIFI_SSID_FALLBACK, WIFI_PASS_FALLBACK) != WL_CONNECTED) {
      WiFi.disconnect();
      display.error(String("Can't connect to the fallback network. Device will not operate."));
      while(1) yield();
    }
  }
  display.clear_message();

  // Serial.println("");
  // Serial.println("WiFi connected");
  // Serial.println("IP address: ");
  // Serial.println(WiFi.localIP());

  UdpMidi.begin(MIDI_CHANNEL_OMNI);
  UdpMidi.turnThruOff();

  set_midi_handlers();

  OrganControl::load_pedal_value();
}

void loop()
{
  const size_t draw_divider = 20;
  static size_t counter = 0;

  const unsigned long ping_timeout = 1000;
  const unsigned long active_sensing_timeout = 300;

  static unsigned long previous_buffer_timeout = 0;


  unsigned long buffer_timeout = OrganControl::get_buffer_value();
  // if (previous_buffer_timeout != buffer_timeout) {
  //   previous_buffer_timeout = buffer_timeout;
  //   display.info(String("Timeout: ") + String(buffer_timeout));
  // }

  // SerialMidi.read();

  Usb.Task();

  if (Usb.getUsbTaskState() == USB_STATE_RUNNING ) {
    if (UsbMidiWrapper) {
      while (UsbMidi.read()) {
      }
    }
  }

  if (ms_since_active_sensing >= active_sensing_timeout) {
    handle_active_sensing();
  }

  if ((message_buffer.size() > 0) && (us_since_packet >= buffer_timeout)) {
    send_packet();
  }

  if (ms_since_ping >= ping_timeout) {
    // Serial.println("Ping:");
    // handle_note_on(1, 48, 80);
    ms_since_ping = 0;
  }

  if (counter++ % draw_divider == 0) {
    display.draw();
  }
}