// #include <hidboot.h>
// #include <Arduino.h>
// #include <ESP8266WiFi.h>
#include <EEPROM.h>


#include "Common.h"
#include "Control.h"

// EEPROM structure
// 0x00 - 0x6C - 27 organ states
// 0x6D - 0x70 - pedal value

static unsigned long buffer_value = 750;

static uint32_t current_switches = 0;
const size_t max_preset_number = 26;
const size_t PEDAL_VALUE_ADDRESS = (max_preset_number + 1) * sizeof(uint32_t);
static int pedal_value = 11;
const size_t EEPROM_SIZE =  PEDAL_VALUE_ADDRESS + sizeof(int);

static int transposition = 0;
const int transposition_limit = 12;
static bool pedal_enabled = false;

void KbdRptParser::PrintKey(uint8_t m, uint8_t key)
{
  MODIFIERKEYS mod;
  *((uint8_t*)&mod) = m;
  Serial.print((mod.bmLeftCtrl   == 1) ? "C" : " ");
  Serial.print((mod.bmLeftShift  == 1) ? "S" : " ");
  Serial.print((mod.bmLeftAlt    == 1) ? "A" : " ");
  Serial.print((mod.bmLeftGUI    == 1) ? "G" : " ");

  Serial.print(" >");
  PrintHex<uint8_t>(key, 0x80);
  Serial.print("< ");

  Serial.print((mod.bmRightCtrl   == 1) ? "C" : " ");
  Serial.print((mod.bmRightShift  == 1) ? "S" : " ");
  Serial.print((mod.bmRightAlt    == 1) ? "A" : " ");
  Serial.println((mod.bmRightGUI    == 1) ? "G" : " ");
};


void KbdRptParser::OnKeyDown(uint8_t mod, uint8_t key)
{
  Serial.print("DN ");
  PrintKey(mod, key);
  uint8_t c = OemToAscii(mod, key);

  // if (c)
    // OnKeyPressed(c);

  MODIFIERKEYS m;
  *((uint8_t*)&m) = mod;

  switch (key) {
    case 0x2A: OrganControl::set_switches(0); break; // Clear
    case 0x29: OrganControl::all_notes_off(); // PANIC!
    case 0x36: OrganControl::pedal_down(); break;
    case 0x37: OrganControl::pedal_up(); break;
    case 0x53: OrganControl::pedal_toggle(); break;
    case 0x4A: OrganControl::transpose_reset(); break;
    case 0x4B: OrganControl::transpose_up(); break;
    case 0x4E: OrganControl::transpose_down(); break;
    // case 0x52: OrganControl::buffer_up(); break;
    // case 0x51: OrganControl::buffer_down(); break;
  }

  // Alphabet keys + space
  if ((key >= 0x04 && key <= 0x1D) || key == 0x2C) {
    uint8_t preset_number = (key == 0x2C) ? max_preset_number : key - 0x04; 
    if (m.bmLeftCtrl == 1 || m.bmRightCtrl == 1) {
      OrganControl::save_switches(preset_number);
    }
    else {
      OrganControl::load_switches(preset_number);
    }  
  }

  // Right numeric keys - pedal
  if (key >= 0x59 && key <= 0x61) {
    OrganControl::toggle_switch(key - 0x59);
  }

  // Numeric keys - Manual 1
  if (key == 0x35) {
    OrganControl::toggle_switch(9);
  }
  if (key >= 0x1E && key <= 0x27) {
    OrganControl::toggle_switch(key - 0x14);
  }

  // Function keys - Manual 2
  if (key >= 0x3A && key <= 0x45) {
    OrganControl::toggle_switch(key - 0x26);
  }
}

/*
void KbdRptParser::OnControlKeysChanged(uint8_t before, uint8_t after) {

  MODIFIERKEYS beforeMod;
  *((uint8_t*)&beforeMod) = before;

  MODIFIERKEYS afterMod;
  *((uint8_t*)&afterMod) = after;

  if (beforeMod.bmLeftCtrl != afterMod.bmLeftCtrl) {
    Serial.println(F("LeftCtrl changed"));
  }
  if (beforeMod.bmLeftShift != afterMod.bmLeftShift) {
    Serial.println(F("LeftShift changed"));
  }
  if (beforeMod.bmLeftAlt != afterMod.bmLeftAlt) {
    Serial.println(F("LeftAlt changed"));
  }
  if (beforeMod.bmLeftGUI != afterMod.bmLeftGUI) {
    Serial.println(F("LeftGUI changed"));
  }

  if (beforeMod.bmRightCtrl != afterMod.bmRightCtrl) {
    Serial.println(F("RightCtrl changed"));
  }
  if (beforeMod.bmRightShift != afterMod.bmRightShift) {
    Serial.println(F("RightShift changed"));
  }
  if (beforeMod.bmRightAlt != afterMod.bmRightAlt) {
    Serial.println(F("RightAlt changed"));
  }
  if (beforeMod.bmRightGUI != afterMod.bmRightGUI) {
    Serial.println(F("RightGUI changed"));
  }
}


void KbdRptParser::OnKeyUp(uint8_t mod, uint8_t key)
{
  Serial.print("UP ");
  PrintKey(mod, key);
}

void KbdRptParser::OnKeyPressed(uint8_t key)
{
  Serial.print("ASCII: ");
  Serial.println((char)key);
};

*/
void send_packet();

void all_notes_off_channel(midi::Channel channel)
{
  const int note_min = 36;
  const int note_max = (channel == 4) ? 62 : 89;
  for (int i = note_min; i <= note_max; ++i) {
    send_note_off(channel, i, 0);
  }
}

void OrganControl::all_notes_off()
{
  int tr = get_transposition();
  int pv = pedal_value;
  transposition = 0;
  pedal_value = -1;
  all_notes_off_channel(2);
  all_notes_off_channel(1);
  all_notes_off_channel(4);
  send_packet();
  transposition = tr;
  pedal_value = pv;
}

unsigned long OrganControl::get_buffer_value()
{
  return buffer_value;
}

unsigned long OrganControl::buffer_up()
{
  return buffer_value += 100;
}

unsigned long OrganControl::buffer_down()
{
  if (buffer_value > 100) {
    buffer_value -= 100;
  }
  return buffer_value;
}

int OrganControl::get_pedal_value()
{
  if (!pedal_enabled) {
    return -1;
  }
  return pedal_value;
}

int OrganControl::load_pedal_value()
{
  EEPROM.begin(EEPROM_SIZE);
  EEPROM.get(PEDAL_VALUE_ADDRESS, pedal_value);
  EEPROM.end();
  return pedal_value;
}

int OrganControl::save_pedal_value()
{
  EEPROM.begin(EEPROM_SIZE);
  EEPROM.put(PEDAL_VALUE_ADDRESS, pedal_value);
  EEPROM.end();
  return pedal_value;
}

bool OrganControl::pedal_toggle()
{
  return pedal_enabled = !pedal_enabled;
}

int OrganControl::pedal_down()
{
  if (pedal_value > 0) {
    --pedal_value;
    save_pedal_value();
  }
  return pedal_value;
}

int OrganControl::pedal_up()
{
  if (pedal_value < 24) {
    ++pedal_value;
    save_pedal_value();
  }
  return pedal_value;
}

uint32_t OrganControl::get_switches()
{
  return current_switches;
}

bool OrganControl::get_switch(uint8_t switch_number)
{
  return bitRead(current_switches, switch_number);
}

int OrganControl::get_transposition()
{
  return transposition;
}

uint32_t OrganControl::load_switches(uint8_t preset_number)
{
  if (preset_number > max_preset_number) {
    return 0;
  }
  uint32_t switches;
  EEPROM.begin(EEPROM_SIZE);
  EEPROM.get(preset_number * sizeof(uint32_t), switches);
  EEPROM.end();
  return set_switches(switches);
  // Serial.printf("Loading switches: %d %d\n", current_switches, preset_number);
  // return current_switches;
}

uint32_t OrganControl::save_switches(uint8_t preset_number)
{
  if (preset_number > max_preset_number) {
    return 0;
  }
  EEPROM.begin(EEPROM_SIZE);
  EEPROM.put(preset_number * sizeof(uint32_t), current_switches);
  EEPROM.end();
  // Serial.printf("Saving switches: %d %d\n", current_switches, preset_number);
  return current_switches;
}

uint32_t OrganControl::set_switches(uint32_t switches) {
  Udp.beginPacket(RECEPTOR_IP, UDP_PORT);
  for (int i = 0; i < sizeof(ORGAN_SWITCHES); ++i) {
    bool state = bitRead(switches, i);
    send_control_change(1, ORGAN_SWITCHES[i], state ? 127 : 0);
    // set_switch(i, state);
  }
  Udp.endPacket();
  return current_switches = switches;
  // return current_switches;
}

bool OrganControl::set_switch(uint8_t switch_number, bool state)
{
  bitWrite(current_switches, switch_number, state);
  // Serial.printf("Setting switch: %d %d\n", switch_number, state);
  return state;
}

bool OrganControl::toggle_switch(uint8_t switch_number) {
  bool state = !get_switch(switch_number);
  handle_control_change(1, ORGAN_SWITCHES[switch_number], state ? 127 : 0);
  return set_switch(switch_number, state);
}

int OrganControl::transpose_down()
{
  // Serial.println(transposition);
  if (transposition > -transposition_limit) {
    --transposition;
  }
  return transposition;
}

int OrganControl::transpose_reset()
{
  // Serial.println(transposition);
  return transposition = 0;
}

int OrganControl::transpose_up()
{
  // Serial.println(transposition);
  if (transposition < transposition_limit) {
    ++transposition;
  }
  return transposition;
}