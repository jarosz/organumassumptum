#pragma once
#include <usbh_midi.h>

class MidiUsb
: protected USBH_MIDI
, public Stream
{
public:
  MidiUsb(USB *usb);

  operator bool() { return this->isMidiFound; }

  void begin(long BaudRate);

  virtual int available();
  virtual int peek();
  virtual int read();
  virtual size_t write(uint8_t);
protected:
  byte message_buffer[3];
  byte message_size;
};