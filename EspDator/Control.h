#pragma once

#include <hidboot.h>

class KbdRptParser : public KeyboardReportParser
{
  void PrintKey(uint8_t mod, uint8_t key);

protected:
  // void OnControlKeysChanged(uint8_t before, uint8_t after);
  void OnKeyDown (uint8_t mod, uint8_t key);
  // void OnKeyUp (uint8_t mod, uint8_t key);
  // void OnKeyPressed(uint8_t key);
};


namespace OrganControl {
	uint32_t get_switches();
	bool get_switch(uint8_t switch_number);
	int get_transposition();
  int get_pedal_value();
  int load_pedal_value();
  int save_pedal_value();
  int pedal_up();
  int pedal_down();
  bool pedal_toggle();

  void all_notes_off();

	uint32_t load_switches(uint8_t preset_number);
	uint32_t save_switches(uint8_t preset_number);
	uint32_t set_switches(uint32_t switches);
	bool set_switch(uint8_t switch_number, bool state);
	bool toggle_switch(uint8_t switch_number);

	int transpose_down();
	int transpose_reset();
	int transpose_up();

  unsigned long get_buffer_value();
  unsigned long buffer_up();
  unsigned long buffer_down();
}