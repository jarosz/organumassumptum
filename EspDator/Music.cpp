#include "Music.h"
#include "Common.h"

MidiUsb::MidiUsb(USB *usb)
: USBH_MIDI{usb}
, message_buffer{0}
, message_size{0}
{

}

int MidiUsb::available()
{
  // Not working for now
  // if (!*this) { // operator bool v0.4
  // if (pUsb->getUsbTaskState()==USB_STATE_RUNNING) {
    // DEBUG_PRINTLN(F("MidiUsb::available() = 0"));
    // return 0;
  // }
  if (this->message_size > 0) {
    return this->message_size;
  }
  this->message_size = this->RecvData(message_buffer);
  return this->message_size;
}

void MidiUsb::begin(long BaudRate)
{
  return; // empty function to compile
}

int MidiUsb::peek()
{
  return this->available()
    ? this->message_buffer[0]
    : -1;
}

int MidiUsb::read()
{
  if (!this->available()) {
    return -1;
  }

  byte output = 0;
  switch (this->message_size) {
    case 1:
      output = this->message_buffer[0];
      this->message_buffer[0] = this->message_buffer[1];
      break;
    case 2:
      output = this->message_buffer[0];
      this->message_buffer[0] = this->message_buffer[1];
      this->message_buffer[1] = this->message_buffer[2];
      break;
    case 3:
      output = this->message_buffer[0];
      this->message_buffer[0] = this->message_buffer[1];
      this->message_buffer[1] = this->message_buffer[2];
      this->message_buffer[2] = 0;
      break;
    default:
      return -1;
  }
  --this->message_size;
  return output;
}

size_t MidiUsb::write(uint8_t)
{
  // TODO
  return 0;
}