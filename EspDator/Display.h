#pragma once

#include <U8g2lib.h>

class Display
{
protected:
  U8G2_SH1106_128X64_NONAME_1_HW_I2C u8g2;

  const int width = 4;
  const int height = 6;
  const int height2 = 8;
  const int margin = 1;

  String message;
  uint16_t message_icon;

public:
  Display();

  void warning(String msg);
  void error(String msg);
  void info(String msg);
  void clear_message();
  void set_message(String msg, uint16_t icon);

  void draw();
  void draw_frame();
  void draw_message();
  void draw_status();
  void draw_switches();

  void draw_setup();

  void setup();
};