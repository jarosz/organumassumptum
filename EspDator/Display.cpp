#include <elapsedMillis.h>
#include <ESP8266WiFi.h>
#include <usbhub.h>

#include "Display.h"

#include "Common.h"
#include "Control.h"

extern USB Usb;

#define LOBYTE(x) ((char*)(&(x)))[0]
#define HIBYTE(x) ((char*)(&(x)))[1]

const byte SWITCH_STATUS[2][8] U8X8_PROGMEM = {
  {
    0b10011001,
    0b11111111,
    0b00000000,
    0b00000000,
    0b00000000,
    0b00000000,
    0b00000000,
    0b00000000 ,        
  },
  {
    0b00011000,
    0b00011000,
    0b00011000,
    0b00011000,
    0b00011000,
    0b00011000,
    0b10011001,
    0b11111111,        
  },
};

static bool usb_hub_connected = false;
static bool hid_keyboard_connected = false;
static bool midi_keyboard_connected = false;

static long signal_quality = -1;

static elapsedMillis ms_since_message;

void update_signal_quality()
{
  if (WiFi.status() == WL_CONNECTED && signal_quality == -1) {
    // TODO: All note off, resend control change
  }
  if (WiFi.status() != WL_CONNECTED) {
    if (signal_quality != -1) {
      // Reset running status
      UdpMidi.sendSongSelect(0);
    }
    signal_quality = -1;
    return;
  }
  long rssi = WiFi.RSSI();

  const long perfect_rssi = -40;
  const long worst_rssi = -85;

  signal_quality = (
    100 * (perfect_rssi - worst_rssi) * (perfect_rssi - worst_rssi) -
    (perfect_rssi - rssi) * (15 * (perfect_rssi - worst_rssi) + 62 * (perfect_rssi - rssi))
  ) / ((perfect_rssi - worst_rssi) * (perfect_rssi - worst_rssi));

  if (signal_quality > 99)
    signal_quality = 99;
  else if (signal_quality < 0)
    signal_quality = 0;
}

uint8_t get_num_configurations(uint8_t addr, uint8_t &num_conf)
{
  USB_DEVICE_DESCRIPTOR buf;
  uint8_t rcode;
  rcode = Usb.getDevDescr( addr, 0, DEV_DESCR_LEN, ( uint8_t *)&buf );
  if (rcode) {
    return (rcode);
  }
  num_conf = buf.bNumConfigurations;
  return 0;
}

uint8_t check_configuration_description_for_connected_devices(uint8_t addr, uint8_t conf)
{
  const size_t BUFSIZE = 256;
  uint8_t buf[BUFSIZE];
  uint8_t* buf_ptr = buf;
  uint8_t rcode;
  uint8_t descr_length;
  uint8_t descr_type;
  uint16_t total_length;
  rcode = Usb.getConfDescr(addr, 0, 4, conf, buf);  //get total length
  LOBYTE( total_length ) = buf[2];
  HIBYTE( total_length ) = buf[3];
  if (total_length > BUFSIZE) {   //check if total length is larger than buffer
    total_length = BUFSIZE;
  }
  rcode = Usb.getConfDescr(addr, 0, total_length, conf, buf); //get the whole descriptor
  while ( buf_ptr < buf + total_length ) { //parsing descriptors
    descr_length = *(buf_ptr);
    descr_type = *(buf_ptr + 1);
    switch (descr_type) {
      case (USB_DESCRIPTOR_INTERFACE):
        USB_INTERFACE_DESCRIPTOR* intf_ptr = (USB_INTERFACE_DESCRIPTOR*)buf_ptr;
        if (intf_ptr->bInterfaceClass == 9 &&
            intf_ptr->bInterfaceSubClass == 0 &&
            intf_ptr->bInterfaceProtocol == 0) {
          usb_hub_connected = true;
        }
        if (intf_ptr->bInterfaceClass == 3 &&
            intf_ptr->bInterfaceSubClass == 1 &&
            intf_ptr->bInterfaceProtocol == 1) {
          hid_keyboard_connected = true;
        }
        if (intf_ptr->bInterfaceClass == 1 &&
            intf_ptr->bInterfaceSubClass == 3 &&
            intf_ptr->bInterfaceProtocol == 0) {
          midi_keyboard_connected = true;
        }
        // return 0;
        break;
    }//switch( descr_type
    buf_ptr = (buf_ptr + descr_length);    //advance buffer pointer
  }//while( buf_ptr <=...
  return (rcode);
}

void check_descriptors_for_connected_devices(uint8_t addr)
{
  uint8_t rcode = 0;
  uint8_t num_conf = 0;

  rcode = get_num_configurations((uint8_t)addr, num_conf);

  for (uint8_t i = 0; i < num_conf; i++) {
    rcode = check_configuration_description_for_connected_devices(addr, i);
  }
}

void check_connected_devices(UsbDevice *pdev)
{
  check_descriptors_for_connected_devices(pdev->address.devAddress);
}

void update_connected_devices()
{
  usb_hub_connected = false;
  hid_keyboard_connected = false;
  midi_keyboard_connected = false;

  if (Usb.getUsbTaskState() == USB_STATE_RUNNING ) {
    Usb.ForEachUsbDevice(&check_connected_devices);
  }
}

Display::Display()
: u8g2{U8G2_R0, 16, 5, 4}
, message_icon{0xBC}
{
}

void Display::setup()
{
  u8g2.begin();  
}

void Display::clear_message()
{
  message = "";
  draw();
}

void Display::set_message(String msg, uint16_t icon)
{
  message = msg;
  message_icon = icon;
  ms_since_message = 0;
  draw();
}

void Display::error(String msg)
{
  set_message(msg, 0x56);
}

void Display::info(String msg)
{
  set_message(msg, 0xBC);
}

void Display::draw()
{
  update_signal_quality();
  update_connected_devices();

  u8g2.firstPage();
  do {
    /* all graphics commands have to appear within the loop body. */
    if (message.length() > 0) {
      draw_message();
    }
    draw_switches();
    draw_status();
    // draw_frame();
  } while (u8g2.nextPage());
}

void Display::draw_frame()
{
  u8g2.drawVLine(108, 0, 46);
  u8g2.drawHLine(0, 46, 109);
}

void Display::draw_message()
{
  const unsigned long message_timeout = 3000;
  if (ms_since_message >= message_timeout) {
    message = "";
  }
  u8g2.setFont(u8g2_font_open_iconic_all_1x_t);
  u8g2.drawGlyph(120, 3*height + 4*height2 + 9*margin, message_icon);
  u8g2.setFont(u8g2_font_tom_thumb_4x6_tf);
  u8g2.setCursor(0, 4*height + 3*height2 + 4*margin);  
  u8g2.print(message.substring(0,30));
  if (message.length() > 30) {
    u8g2.setCursor(0, 5*height + 3*height2 + 4*margin);  
    u8g2.print(message.substring(30,60));
  }
  if (message.length() > 60) {
    u8g2.setCursor(0, 6*height + 3*height2 + 4*margin);  
    u8g2.print(message.substring(60,90));    
  }
}

void Display::draw_status()
{
  if (signal_quality != -1) {
    u8g2.setFont(u8g2_font_tom_thumb_4x6_te);
    u8g2.setCursor(120, height);
    u8g2.print(signal_quality);
  }
  else {
    u8g2.setFont(u8g2_font_open_iconic_all_1x_t);
    u8g2.drawGlyph(119, height2, 0x11B);
  }
  u8g2.setFont(u8g2_font_open_iconic_all_1x_t);
  u8g2.drawGlyph(110, height2, 0xF7);
  // u8g2.drawGlyph(119, 8, 0x53);
  u8g2.drawGlyph(110, 2*height2 + margin, 0xAA);
  u8g2.drawGlyph(119, 2*height2 + margin, (usb_hub_connected ? 0x73 : 0x11B));
  u8g2.drawGlyph(110, 3*height2 + 2*margin, 0x6B);
  u8g2.drawGlyph(119, 3*height2 + 2*margin, (hid_keyboard_connected ? 0x73 : 0x11B));
  u8g2.drawGlyph(110, 4*height2 + 3*margin, 0xE1);
  u8g2.drawGlyph(119, 4*height2 + 3*margin, (midi_keyboard_connected ? 0x73 : 0x11B));
  u8g2.drawGlyph(110, 5*height2 + 4*margin, 0x8E);
  if (OrganControl::get_pedal_value() == -1) {
    u8g2.drawGlyph(119, 5*height2 + 4*margin, 0x11B);
  }
  else {
    u8g2.setFont(u8g2_font_tom_thumb_4x6_tf);
    u8g2.setCursor(120, 4*height2 + height + 4*margin);
    u8g2.print(OrganControl::get_pedal_value() + 1);
  }
}

void Display::draw_switches()
{
  const size_t num_switches = sizeof(ORGAN_SWITCHES);
  const size_t limits[3] = { 12, 11, 9 };

  int j = 0;
  int k = 2;
  for (int i = 0; i < num_switches; ++i) {
    u8g2.drawXBMP(2 * j * width + j * margin, (k+1) * height + (k) * height2 + (k+1) * margin, height2, height2, SWITCH_STATUS[OrganControl::get_switch(i)]);
    const int extra_margin = (i < 9) ? 3 : 1;
    u8g2.setFont(u8g2_font_tom_thumb_4x6_tf);
    u8g2.setCursor(2 * j * width + j * margin + extra_margin, (k+1) * height + (k) * height2 + k * margin);
    u8g2.print(i+1);

    ++j; // next column
    if (j == limits[k]) {
      j = 0; // first column
      --k;   // next row
    }
  }
  u8g2.setCursor(87, 3 * height + 2 * height2 + 2 * margin);
  u8g2.print("T:");
  u8g2.print(OrganControl::get_transposition());
}